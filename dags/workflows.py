import os
from datetime import datetime

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

default_args = {
    "owner": "elodie-molitor",
    "start_date": datetime.now(),
    "retries": 5,
}

os.environ["IMPORT_FOLDER"] = "/home/airflow/project/data/raw"
os.environ["EXPORT_FOLDER"] = "/home/airflow/project/data/processed"
os.environ["TEMP_FOLDER"] = "/home/airflow/project/data/interim"

dag = DAG(
    "servier-pipeline", default_args=default_args, schedule_interval="@once"
)

with dag:

    from servier.preprocessing.clean_data_pipeline import (
        preprocessing_clinical_trials,
        preprocessing_drugs,
        preprocessing_pubmed,
    )
    from servier.process.process_pipeline import (
        process_clinical_trials,
        process_final,
        process_pubmed,
    )

    clean_1 = PythonOperator(
        task_id="clean_clinical_trials",
        python_callable=preprocessing_clinical_trials,
    )

    clean_2 = PythonOperator(
        task_id="clean_drugs", python_callable=preprocessing_drugs
    )

    clean_3 = PythonOperator(
        task_id="clean_pubmed", python_callable=preprocessing_pubmed
    )

    process_1 = PythonOperator(
        task_id="process_pubmed", python_callable=process_pubmed
    )

    process_2 = PythonOperator(
        task_id="process_clinical_trials",
        python_callable=process_clinical_trials,
    )

    process_3 = PythonOperator(
        task_id="process_final", python_callable=process_final
    )

clean_1 >> process_2
clean_2 >> [process_1, process_2]
clean_3 >> process_1
process_1 >> process_3
process_2 >> process_3
