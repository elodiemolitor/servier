import unittest

import pandas as pd

from servier.preprocessing.clean_data import clean_drugs


class CleanDataTests(unittest.TestCase):
    def test_clean_drugs(self):
        df = pd.DataFrame(
            {
                "atccode": ["A", "B", "C"],
                "drug": ["DIPHENHYDRAMINE", "TETRACYCLINE", "ETHANOL"],
            }
        )
        df_expected = pd.DataFrame(
            {
                "atccode": ["A", "B", "C"],
                "drug": ["diphenhydramine", "tetracycline", "ethanol"],
            }
        )
        self.assertTrue(clean_drugs(df).equals(df_expected))


if __name__ == "__main__":
    unittest.main()
