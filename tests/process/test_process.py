import unittest

import pandas as pd

from servier.process.process import add_drug_to_df, get_drugs_in_title


class ProcessTests(unittest.TestCase):
    def test_get_drugs_in_title(self):
        x = "use of diphenhydramine as an adjunctive sedative for colonoscopy in patients chronically on opioids"  # noqa: E501
        list_drugs = [
            "diphenhydramine",
            "tetracycline",
            "ethanol",
            "atropine",
            "epinephrine",
            "isoprenaline",
            "betamethasone",
        ]
        list_expected = ["diphenhydramine"]
        self.assertEqual(get_drugs_in_title(x, list_drugs), list_expected)

        x = "an adjunctive sedative for colonoscopy in patients chronically on opioids"  # noqa: E501
        list_expected = []
        self.assertEqual(get_drugs_in_title(x, list_drugs), list_expected)

        x = "use of diphenhydramine and tetracycline an adjunctive sedative for colonoscopy in patients chronically on opioids"  # noqa: E501
        list_expected = ["diphenhydramine", "tetracycline"]
        self.assertEqual(get_drugs_in_title(x, list_drugs), list_expected)

    def test_add_drug_to_df(self):
        df = pd.DataFrame(
            {
                "title": [
                    "blabla drug1 blabla drug2",
                    "blabla drug",
                    "blabla drug3",
                ]
            }
        )
        drugs = pd.DataFrame({"drug": ["drug1", "drug2", "drug3"]})
        df_expected = pd.DataFrame(
            {
                "title": [
                    "blabla drug1 blabla drug2",
                    "blabla drug",
                    "blabla drug3",
                ],
                "list_drugs": [["drug1", "drug2"], [], ["drug3"]],
            }
        )
        self.assertTrue(add_drug_to_df(drugs, df, "title").equals(df_expected))


if __name__ == "__main__":
    unittest.main()
