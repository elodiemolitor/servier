import unittest

from servier.utilities.cleaner import remove_special_characters


class CleanerTests(unittest.TestCase):
    def test_remove_special_characters_with_no_change(self):
        x = "Hôpitaux Universitaires de Genève"
        self.assertEqual(remove_special_characters(x), x)

    def test_remove_special_characters_with_special_characters(self):
        x = "Preemptive Infiltration With Betamethasone and Ropivacaine for Postoperative Pain in Laminoplasty or \\xc3\\xb1 Laminectomy"  # noqa: E501
        x_expected = "Preemptive Infiltration With Betamethasone and Ropivacaine for Postoperative Pain in Laminoplasty or  Laminectomy"  # noqa: E501
        self.assertEqual(remove_special_characters(x), x_expected)


if __name__ == "__main__":
    unittest.main()
