import os

from servier.preprocessing.clean_data_pipeline import (
    preprocessing_clinical_trials,
    preprocessing_drugs,
    preprocessing_pubmed,
)
from servier.process.process import find_journal_mention_most_drugs
from servier.process.process_pipeline import (
    process_clinical_trials,
    process_final,
    process_pubmed,
)
from servier.utilities.exporter import save_txt
from servier.utilities.importer import load_from_json

# Pre-processing
preprocessing_drugs()
preprocessing_pubmed()
preprocessing_clinical_trials()

# Process
process_pubmed()
process_clinical_trials()
process_final()


# Find journal which mentions the most different drugs
df_json = load_from_json(os.environ["EXPORT_FOLDER"] + "/results.json")
journal_mention_most_drugs = find_journal_mention_most_drugs(df_json)
save_txt(
    journal_mention_most_drugs,
    os.environ["EXPORT_FOLDER"] + "/journal_mention_most_drugs.txt",
)
