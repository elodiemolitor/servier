install: 
	poetry build
	find dist -name '*.whl' -exec pip install --upgrade {} \;

lint_format:
	isort .
	black .
	flake8 .
	find . -type f -name "*.py" | xargs pylint 

run_local:
	python main.py

run_docker:
	docker compose build
	docker compose up -d

get_result_dag:
	docker cp servier_airflow-worker_1:/home/airflow/project/data/processed/results.json ./data/processed/results-dag.json

stop_docker:
	docker compose down

test:
	python -m unittest discover