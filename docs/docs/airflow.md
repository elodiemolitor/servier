## Apache Airflow

Apache Airflow est une plateforme permettant de plannifier un flux de travail pour traiter des données.

On peut créer des workflows sous la forme de DAG (Directed Acyclic Graphs), ce qui permet de définir un ordre et des dépendances entre les tâches à réaliser. On peut également préciser la fréquence d'exécution du DAG (par exemple : toutes les 5 minutes ou tous les jours à partir d'une certaine date).


Voici le graphique des dépendances entre les tâches réalisées par Airflow pour ce projet :
![Graphe des dépendances des tâches réalisées par Airflow](img/graphe_airflow.png)

On peut voir que :

- les tâches "clean_clinical_trials", "clean_drugs" et "clean_pubmed" peuvent être réalisés en parallèle.

- "process_clinical_trials" dépend de "clean_drugs" et "clean_clinical_trials" pour pouvoir se lancer.

- "process_pubmed" dépend de "clean_drugs" et "clean_pubmed" pour pouvoir se lancer.

- "process_clinical_trials" et "process_pubmed" peuvent être réalisés en parallèle.

- "process_final" a besoin que "process_clinical_trials" et "process_pubmed" soient finis pour se lancer.
