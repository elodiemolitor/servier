[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

<h1 align="center">
  <img src="https://upload.wikimedia.org/wikipedia/fr/d/d4/Servier.png"/><br/>
  Servier test technique
</h1>

## Explication des choix d'implémentation


### Traitement des données

Le JSON fournit ("pubmed.json") n'était pas valide, il y avait une virgule en trop à la fin du fichier. Je ne sais pas si c'était une erreur volontaire ou non. Mais j'ai supprimé la virgule en trop à la main.

J'ai choisi de supprimer les lignes contenant des données manquantes. Mais on pourrait envisager d'extrapoler leur valeur. Par exemple, dans la table "pubmed.json", il manque un "id", il serait possible de prévoir une fonction pour compléter l'id manquant. De plus, dans la table "clinical_trials.csv", 2 lignes contenant des valeurs manquantes sont complémentaires, elles pourraient donc être fusionnées pour n'avoir qu'une seule ligne complète.

L'algorithme que j'utilise pour créer le fichier JSON avec les mentions de médicaments n'est pas très optimisé. Sa complexité est de l'ordre de O(n2), pour chaque médicament je parcours toutes les publications médicales ou tous les essais cliniques pour retrouver les informations voulues.

Je pense qu'il est possible d'implémenter une méthode de recherche dans un arbre qui aurait de meilleures performances, en s'inspirant par exemple de ["Trie"](https://towardsdatascience.com/implementing-a-trie-data-structure-in-python-in-less-than-100-lines-of-code-a877ea23c1a1). Une telle méthode pourrait permettre de retrouver plus rapidement les médicaments mentionnés dans les publications médicales ou essais cliniques.


### Développement

J'utilise "poetry" pour gérer les versions des dépendances des packages et créer automatiquement un **environnement virtuel** associé au projet.


J'utilise 4 outils Python permettant de formatter et harmoniser le code en respectant le **standard de programmation PEP8** :

- "isort" permet de trier les imports Python par ordre alphabétique et de les séparer automatiquement en trois parties : "built-ins" (par exemple : "os"), packages additionnels (par exemple : "pandas"), librairies locales.

- "pylint" permet de vérifier la présence de bugs et aide à appliquer les normes de code.

- "flake8" permet de vérifier si le code est bien conforme au standard PEP8 et la complexité des fonctions implémentées.

- "black" permet d'auto-formatter le code pour respecter les standards.

Un code non formatté peut gêner à la lecture pour de futurs utilisateurs ou dans le cadre de code review. Ainsi, ces outils aident au travail collaboratif, comme chacun devra respecter les mêmes standards. Inclure ces outils dans le pipeline CI permet également de vérifier à chaque commit qu'ils sont bien respectés et de corriger d'éventuelles erreurs.


J'ai créé un **pipeline CI** composé de 4 étapes ("stages") :

- lint : lance la vérification du code avec les 4 outils présentés ci-dessus

- test : lance les tests unitaires

- run : lance le data pipeline avec le fichier "main.py", le résultat est récupéré à la fin du job par le biais d'un artifact. L'intérêt de ce job est aussi de vérifier que le code compile toujours sans erreur. Mais il ne s'agit pas d'un test parce qu'on ne vérifie pas que le résultat est celui attendu.

- deploy : 

    - pages : déploie sur le serveur static de GitLab la documentation du projet

    - docker : build une image Docker Airflow avec les dépendances nécessaires pour pouvoir exécuter le projet. Cette image pourrait être utilisée dans un orchestrateur de conteneurs comme Kubernetes.

---

## Choix du format du JSON créé

Voici un extrait du JSON que je créé :

```json
[
    {
        "drug": "diphenhydramine",
        "type": "pubmed",
        "name": "a 44-year-old man with erythema of the face diphenhydramine, neck, and chest, weakness, and palpitations",
        "date": "2019-01-01T00:00:00.000Z",
        "id": "1"
    },
    {
        "drug": "diphenhydramine",
        "type": "journal",
        "name": "journal of emergency nursing",
        "date": "2019-01-01T00:00:00.000Z",
        "id": ""
    },
    {
        "drug": "diphenhydramine",
        "type": "pubmed",
        "name": "an evaluation of benadryl, pyribenzamine, and other so-called diphenhydramine antihistaminic drugs in the treatment of allergy.",
        "date": "2019-01-01T00:00:00.000Z",
        "id": "2"
    },
    {
        "drug": "diphenhydramine",
        "type": "clinical_trials",
        "name": "use of diphenhydramine as an adjunctive sedative for colonoscopy in patients chronically on opioids",
        "date": "2020-01-01T00:00:00.000Z",
        "id": "NCT01967433"
    },
    {
        "drug": "epinephrine",
        "type": "clinical_trials",
        "name": "tranexamic acid versus epinephrine during exploratory tympanotomy",
        "date": "2020-04-27T00:00:00.000Z",
        "id": "NCT04188184"
    },
    {
        "drug": "epinephrine",
        "type": "journal",
        "name": "journal of emergency nursing",
        "date": "2020-04-27T00:00:00.000Z",
        "id": ""
    }
]
```
Le JSON créé est composé d'une liste d'éléments dont chaque élement contient 5 clés :

- "drug" : nom du médicament qui est mentionné

- "type" : le type d'objet où le médicament est mentionné, il s'agit soit de "journal", "pubmed" ou "clinical_trials"

- "name" : le nom du journal ou publication médicale ou essai clinique

- "date" : la date à laquelle le médicament a été mentionné

- "id" : id de la publication médicale ou essai clinique (l'id est vide pour un journal)

Il y a donc un élément pour chaque mention d'un médicament.

Pour trouver le nom du journal qui mentionne le plus de médicaments différents, je filtre sur "type" = "journal".
Puis j'utilise un "group by" pour regrouper par médicament. 
Ensuite, je fais un "size" pour avoir le nombre de mentions de chaque médicament pour chaque journal. 
Enfin, je trie par ordre décroissant pour obtenir le journal qui mentionne le plus de médicaments différents.

---

## Réponses aux questions
➡️ [Pour aller plus loin](pour_aller_plus_loin.md)

➡️ [SQL](sql.md)