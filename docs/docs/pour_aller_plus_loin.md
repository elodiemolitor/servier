## Pour aller plus loin

Pour de grosses volumétries de données, les données ne seront pas dans le projet mais stockées ailleurs (dans le cloud par exemple). Il faudra donc importer les données depuis l'endroit où elles sont stockées. De même, les exports de données ne se feront pas non plus dans le projet ou dans un dossier de la machine courante, il faudra les exporter dans le cloud.

Il faudra donc modifier dans le code les fonctions "load_from_csv", "load_from_json", "save_csv" et "save_json" qui permettent d'importer et exporter les données.

Il est possible de parallèliser certaines étapes du pipeline pour optimiser les temps de traitement. Par exemple, avec Apache Airflow on peut définir des jobs et les dépendances entre eux, afin de déterminer lesquels peuvent être exécutés en parallèle et dans quel ordre. Une implémentation d'Airflow est disponible dans le projet, voir [ici](airflow.md) pour plus d'informations.

On pourrait également faire du MapReduce en découpant les données pour les traiter en parallèle avec Hadoop.