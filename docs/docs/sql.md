## Première partie du test

```sql
SELECT date, SUM(prod_price*prod_qty) AS ventes
FROM TRANSACTION 
WHERE date BETWEEN '01/01/2019' AND '12/31/2019'
GROUP BY date
ORDER BY date
```

## Seconde partie du test

```sql
SELECT  
    client_id, 
    SUM(CASE WHEN product_type = 'MEUBLE' THEN prod_price*prod_qty ELSE 0 END) AS ventes_meuble,
    SUM(CASE WHEN product_type = 'DECO' THEN prod_price*prod_qty ELSE 0 END) AS ventes_deco
FROM TRANSACTION
LEFT JOIN PRODUCT_NOMENCLATURE ON TRANSACTION.prod_id = PRODUCT_NOMENCLATURE.product_id
WHERE date BETWEEN '01/01/2019' AND '12/31/2019' 
GROUP BY client_id
```