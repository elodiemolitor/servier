FROM apache/airflow:2.2.3-python3.8
USER airflow
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
RUN mkdir /home/airflow/project
COPY --chown=airflow:root . /home/airflow/project/
RUN cd /home/airflow/project &&\
    source /home/airflow/.poetry/env &&\
    poetry build &&\
    find dist -name '*.whl' -exec pip install --upgrade {} \;