import pandas as pd
from pandas.core.frame import DataFrame


def load_from_csv(path: str) -> DataFrame:
    """Load data from a csv.

    Args:
        path (str): path to the file to load

    Returns:
        df (DataFrame): data loaded
    """

    df = pd.read_csv(path)
    return df


def load_from_json(path: str, dtype: dict = None) -> DataFrame:
    """Load data from a json.

    Args:
        path (str): path to the file to load
        dtype (dict): dtype of columns

    Returns:
        df (DataFrame): data loaded
    """
    df = pd.read_json(path, dtype=dtype)
    return df
