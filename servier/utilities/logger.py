import logging


class Logger:
    format = (
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"  # noqa: E501
    )

    @staticmethod
    def get_logger(name: str) -> logging.Logger:
        console_formatter = logging.Formatter(Logger.format)
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.INFO)
        console_handler.setFormatter(console_formatter)

        logger = logging.getLogger(name)
        logger.addHandler(console_handler)
        logger.setLevel(logging.DEBUG)

        return logger
