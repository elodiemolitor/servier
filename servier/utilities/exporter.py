from pandas.core.frame import DataFrame


def save_csv(df: DataFrame, path: str) -> None:
    """Save the DataFrame df in csv format to the given path

    Args:
        df (DataFrame): DataFrame to save
        path (str): path where to save the DataFrame
    """
    df.to_csv(path, index=False)


def save_json(df: DataFrame, path: str) -> None:
    """Save the DataFrame df in JSON format to the given path

    Args:
        df (DataFrame): DataFrame to save
        path (str): path where to save the DataFrame
    """
    df.to_json(orient="records", date_format="iso", path_or_buf=path)


def save_txt(text: str, path: str) -> None:
    """Save text in txt format to the given path

    Args:
        text (str): text to save
        path (str): path where to save the DataFrame
    """
    with open(path, "w") as f:
        f.write(text)
