import re


def remove_special_characters(x: str) -> str:
    """Remove special characters that start with '\\x'
    followed by 2 alphanumeric characters.

    Args:
        x ([type]): [description]

    Returns:
        str: [description]
    """
    return re.sub(r"\\x\w{2}", "", x)
