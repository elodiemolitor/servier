import os

import dotenv

dotenv.load_dotenv(override=True)

IMPORT_FOLDER = os.environ["IMPORT_FOLDER"]
TEMP_FOLDER = os.environ["TEMP_FOLDER"]
EXPORT_FOLDER = os.environ["EXPORT_FOLDER"]
