from servier.configuration.constant import EXPORT_FOLDER, TEMP_FOLDER
from servier.process.process import add_drug_to_df, create_drug_mentioned
from servier.utilities.exporter import save_json
from servier.utilities.importer import load_from_json
from servier.utilities.logger import Logger

LOGGER = Logger.get_logger(__name__)


def process_pubmed() -> None:
    """Process pubmed data"""
    LOGGER.info("Begin process pubmed data")
    drugs_cleaned = load_from_json(TEMP_FOLDER + "/drugs_cleaned.json")
    pubmed_cleaned = load_from_json(TEMP_FOLDER + "/pubmed_cleaned.json")
    pubmed_processed = add_drug_to_df(drugs_cleaned, pubmed_cleaned, "title")
    save_json(pubmed_processed, TEMP_FOLDER + "/pubmed_processed.json")
    LOGGER.info("End process pubmed data")


def process_clinical_trials() -> None:
    """Process clinical_trials data"""
    LOGGER.info("Begin process clinical_trials data")
    drugs_cleaned = load_from_json(TEMP_FOLDER + "/drugs_cleaned.json")
    clinical_trials_cleaned = load_from_json(
        TEMP_FOLDER + "/clinical_trials_cleaned.json"
    )
    clinical_trials_processed = add_drug_to_df(
        drugs_cleaned, clinical_trials_cleaned, "scientific_title"
    )
    save_json(
        clinical_trials_processed,
        TEMP_FOLDER + "/clinical_trials_processed.json",
    )
    LOGGER.info("End process clinical_trials data")


def process_final() -> None:
    """Final process to create a JSON file."""
    LOGGER.info("Begin final process")
    pubmed_processed = load_from_json(
        TEMP_FOLDER + "/pubmed_processed.json", dtype={"id": str}
    )
    clinical_trials_processed = load_from_json(
        TEMP_FOLDER + "/clinical_trials_processed.json"
    )
    df = create_drug_mentioned(pubmed_processed, clinical_trials_processed)
    save_json(df, EXPORT_FOLDER + "/results.json")
    LOGGER.info("End final process")
