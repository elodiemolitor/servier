from typing import List

import pandas as pd
from pandas.core.frame import DataFrame


def add_drug_to_df(
    drugs: DataFrame, df: DataFrame, col_title: str
) -> DataFrame:
    """Add a column with the list of drugs mentioned in the column col_title
    of df.

    Args:
        drugs (DataFrame): DataFrame containing drug data
        df (DataFrame): DataFrame to which we want to add a column
        with the mentioned drugs
        col_title (str): name of the column that contains the title

    Returns:
        DataFrame: DataFrame with a new column 'list_drugs' with the list of
        mentioned drugs
    """

    df["list_drugs"] = df[col_title].apply(
        lambda x: get_drugs_in_title(x, drugs["drug"])
    )

    return df


def get_drugs_in_title(title: str, list_drugs: List[str]) -> List[str]:
    """Return the list of drugs mentioned in the title.

    Args:
        title (str): title
        list_drugs (List[str]): list of drugs to search

    Returns:
        List[str]: list of drugs mentioned in title
    """

    list_drugs_in_title = []
    for drug in list_drugs:
        if drug in title:
            list_drugs_in_title.append(drug)
    return list_drugs_in_title


def create_drug_mentioned(
    pubmed: DataFrame, clinical_trials: DataFrame
) -> DataFrame:
    """Create a DataFrame with all the drugs mentioned by journals, clinical
    trials or medical publications.

    Args:
        pubmed (DataFrame): DataFrame containing pubmed data
        clinical_trials (DataFrame): DataFrame containing clinical trials data

    Returns:
        DataFrame: DataFrame with all the drugs mentioned by journals, clinical
        trials or medical publications
    """
    df = pd.DataFrame(columns=["drug", "type", "name", "date", "id"])
    for x in range(len(pubmed)):
        for drug in pubmed["list_drugs"].iloc[x]:
            df.loc[len(df)] = [
                drug,
                "pubmed",
                pubmed["title"].iloc[x],
                pubmed["date"].iloc[x],
                pubmed["id"].iloc[x],
            ]
            df.loc[len(df)] = [
                drug,
                "journal",
                pubmed["journal"].iloc[x],
                pubmed["date"].iloc[x],
                "",
            ]
    for x in range(len(clinical_trials)):
        for drug in clinical_trials["list_drugs"].iloc[x]:
            df.loc[len(df)] = [
                drug,
                "clinical_trials",
                clinical_trials["scientific_title"].iloc[x],
                clinical_trials["date"].iloc[x],
                clinical_trials["id"].iloc[x],
            ]
            df.loc[len(df)] = [
                drug,
                "journal",
                clinical_trials["journal"].iloc[x],
                clinical_trials["date"].iloc[x],
                "",
            ]

    df.drop_duplicates(inplace=True)

    return df


def find_journal_mention_most_drugs(df_json: DataFrame) -> str:
    """Find the journal which mentions the most different drugs.

    Args:
        df_json (DataFrame): DataFrame of the JSON file created by the data
        pipeline

    Returns:
        str: name of the journal which mentions the most different drugs
    """
    df_journal = df_json[df_json["type"] == "journal"][["drug", "name"]]
    df_journal.drop_duplicates(inplace=True)
    df_journal_group_size = df_journal.groupby(["name"]).size()
    df_journal_group_size.sort_values(ascending=False, inplace=True)

    return df_journal_group_size.index[0]
