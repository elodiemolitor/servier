from servier.configuration.constant import IMPORT_FOLDER, TEMP_FOLDER
from servier.preprocessing.clean_data import (
    clean_clinical_trials,
    clean_drugs,
    clean_pubmed,
    merge_df,
)
from servier.utilities.exporter import save_json
from servier.utilities.importer import load_from_csv, load_from_json
from servier.utilities.logger import Logger

LOGGER = Logger.get_logger(__name__)


def preprocessing_drugs() -> None:
    """Preprocessing of drugs data"""
    LOGGER.info("Begin preprocessing drugs data")
    drugs = load_from_csv(IMPORT_FOLDER + "/drugs.csv")
    drugs_cleaned = clean_drugs(drugs)
    save_json(drugs_cleaned, TEMP_FOLDER + "/drugs_cleaned.json")
    LOGGER.info("End preprocessing drugs data")


def preprocessing_pubmed() -> None:
    """Preprocessing of pubmed data"""
    LOGGER.info("Begin preprocessing pubmed data")
    pubmed_csv = load_from_csv(IMPORT_FOLDER + "/pubmed.csv")
    pubmed_json = load_from_json(IMPORT_FOLDER + "/pubmed.json")
    pubmed = merge_df(pubmed_csv, pubmed_json)
    pubmed_cleaned = clean_pubmed(pubmed)
    save_json(pubmed_cleaned, TEMP_FOLDER + "/pubmed_cleaned.json")
    LOGGER.info("End preprocessing pubmed data")


def preprocessing_clinical_trials() -> None:
    """Preprocessing of clinical_trials data"""
    LOGGER.info("Begin preprocessing clinical_trials data")
    clinical_trials = load_from_csv(IMPORT_FOLDER + "/clinical_trials.csv")
    clinical_trials_cleaned = clean_clinical_trials(clinical_trials)
    save_json(
        clinical_trials_cleaned, TEMP_FOLDER + "/clinical_trials_cleaned.json"
    )
    LOGGER.info("End preprocessing clinical_trials data")
