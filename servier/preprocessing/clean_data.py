import pandas as pd
from pandas.core.frame import DataFrame

from servier.utilities.cleaner import remove_special_characters


def clean_clinical_trials(clinical_trials: DataFrame) -> DataFrame:
    """Clean the data clinical_trials.

    Drop rows with missing values. Convert dates to datetime.
    Remove special characters and lowercase the string columns.

    Args:
        clinical_trials (DataFrame): [description]

    Returns:
        DataFrame: [description]
    """

    clinical_trials.dropna(inplace=True)
    clinical_trials["date"] = pd.to_datetime(clinical_trials["date"])
    clinical_trials["journal"] = clinical_trials["journal"].apply(
        remove_special_characters
    )
    clinical_trials["journal"] = clinical_trials["journal"].str.lower()
    clinical_trials["scientific_title"] = clinical_trials[
        "scientific_title"
    ].str.lower()
    clinical_trials["scientific_title"] = clinical_trials[
        "scientific_title"
    ].apply(remove_special_characters)

    return clinical_trials


def merge_df(df1: DataFrame, df2: DataFrame) -> DataFrame:
    """Merge the two DataFrame.

    Args:
        df1 (DataFrame): fisrt DataFrame to merge
        df2 (DataFrame): second DataFrame to merge

    Returns:
        DataFrame: merged DataFrame
    """
    df_merged = pd.concat([df1, df2], axis=0)
    return df_merged


def clean_pubmed(pubmed: DataFrame) -> DataFrame:
    """Clean the data pubmed.

    Drop rows with missing values. Convert dates to datetime.
    Remove special characters and lowercase the string columns.

    Args:
        pubmed (DataFrame): DataFrame containing pubmed data

    Returns:
        DataFrame: DataFrame containing cleaned pubmed data
    """
    pubmed.dropna(inplace=True)
    pubmed = pubmed.astype({"id": str})
    # to drop rows with empty id
    pubmed = pubmed[pubmed["id"].astype(bool)]
    pubmed["date"] = pd.to_datetime(pubmed["date"])
    pubmed["title"] = pubmed["title"].apply(remove_special_characters)
    pubmed["title"] = pubmed["title"].str.lower()
    pubmed["journal"] = pubmed["journal"].apply(remove_special_characters)
    pubmed["journal"] = pubmed["journal"].str.lower()

    return pubmed


def clean_drugs(drugs: DataFrame) -> DataFrame:
    """Clean the data drugs.

    Turn into lowercase the string columns.

    Args:
        drugs (DataFrame): DataFrame containing drug data

    Returns:
        DataFrame: DataFrame containing cleaned drug data
    """
    drugs["drug"] = drugs["drug"].str.lower()
    return drugs
